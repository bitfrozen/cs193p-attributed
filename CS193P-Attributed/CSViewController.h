//
//  CSViewController.h
//  CS193P-Attributed
//
//  Created by Antanas Domarkas on 2013-12-25.
//  Copyright (c) 2013 Curonian Software. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CSViewController : UIViewController

@end
