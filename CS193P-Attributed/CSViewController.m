//
//  CSViewController.m
//  CS193P-Attributed
//
//  Created by Antanas Domarkas on 2013-12-25.
//  Copyright (c) 2013 Curonian Software. All rights reserved.
//

#import "CSViewController.h"

@interface CSViewController ()

@property(weak, nonatomic) IBOutlet UITextView *label;
@property(weak, nonatomic) IBOutlet UIStepper *selectedWordStepper;
@property(weak, nonatomic) IBOutlet UILabel *selectedWordLabel;

- (NSArray *)wordList;
- (NSString *)selectedWord;
- (IBAction)updateSelectedWord;
- (void)addLabelAttributes:(NSDictionary *)attributes forRange:(NSRange)range;
- (void)addSelectedWordAttributes:(NSDictionary *)attributes;
- (IBAction)underline;
- (IBAction)ununderline;
- (IBAction)changeColor:(UIButton *)sender;
- (IBAction)changeFont:(UIButton *)sender;
- (IBAction)outline;
- (IBAction)unoutline;

@end

@implementation CSViewController

@synthesize label = _label;
@synthesize selectedWordStepper = _selectedWordStepper;
@synthesize selectedWordLabel = _selectedWordLabel;

- (NSArray *)wordList
{
    NSArray *wordList
        = [[self.label.attributedText string] componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];

    if ([wordList count]) {
        return wordList;
    } else {
        return @[ @"" ];
    }
}

- (NSString *)selectedWord
{
    return [self wordList][(NSInteger)self.selectedWordStepper.value];
}

- (IBAction)updateSelectedWord
{
    self.selectedWordStepper.maximumValue = [[self wordList] count] - 1;
    self.selectedWordLabel.text = [self selectedWord];
}

- (void)addLabelAttributes:(NSDictionary *)attributes forRange:(NSRange)range
{
    if (range.location != NSNotFound) {
        NSMutableAttributedString *mutableString = [self.label.attributedText mutableCopy];
        [mutableString addAttributes:attributes range:range];
        self.label.attributedText = mutableString;
    }
}

- (void)addSelectedWordAttributes:(NSDictionary *)attributes
{
    NSRange range = [[self.label.attributedText string] rangeOfString:[self selectedWord]];
    [self addLabelAttributes:attributes forRange:range];
}

- (IBAction)underline
{
    [self addSelectedWordAttributes:@{ NSUnderlineStyleAttributeName : @(NSUnderlineStyleSingle) }];
}

- (IBAction)ununderline
{
    [self addSelectedWordAttributes:@{ NSUnderlineStyleAttributeName : @(NSUnderlineStyleNone) }];
}

- (IBAction)changeColor:(UIButton *)sender
{
    [self addSelectedWordAttributes:@{ NSForegroundColorAttributeName : sender.backgroundColor }];
}

- (IBAction)changeFont:(UIButton *)sender
{
    CGFloat fontSize = [UIFont systemFontSize];
    NSDictionary *attributes = [self.label.attributedText attributesAtIndex:0 effectiveRange:NULL];
    UIFont *existingFont = attributes[NSFontAttributeName];
    if (existingFont) {
        fontSize = existingFont.pointSize;
    }
    UIFont *font = [sender.titleLabel.font fontWithSize:fontSize];
    [self addSelectedWordAttributes:@{ NSFontAttributeName : font }];
}

- (IBAction)outline
{
    [self addSelectedWordAttributes:@{ NSStrokeWidthAttributeName : @(5) }];
}

- (IBAction)unoutline
{
    [self addSelectedWordAttributes:@{ NSStrokeWidthAttributeName : @(0) }];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self updateSelectedWord];
}

@end
